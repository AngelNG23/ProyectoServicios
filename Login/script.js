function toggleResetPswd(e){
    e.preventDefault();
    $('#FormularioGeneral .FormularioInicio').toggle() // display:block or none
    $('#FormularioGeneral .form-reset').toggle() // display:block or none
}

function toggleSignUp(e){
    e.preventDefault();
    $('#FormularioGeneral .FormularioInicio').toggle(); // display:block or none
    $('#FormularioGeneral .form-signup').toggle(); // display:block or none
}

$(()=>{
    // Login Register Form
    $('#FormularioGeneral #forgot_pswd').click(toggleResetPswd);
    $('#FormularioGeneral #cancel_reset').click(toggleResetPswd);
    $('#FormularioGeneral #btn-signup').click(toggleSignUp);
    $('#FormularioGeneral #cancel_signup').click(toggleSignUp);
})